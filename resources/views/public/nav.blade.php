<div class="pad-20-top-n-bottom theme-bg">
    <div class="container flex-on flex-centre flex-distribute">
        <div>
            <img class="icon margin-10-right" src="{{ asset('icons/home.png') }}" alt="">
        </div>
        <div class="flex-on flex-centre">
                <span class="site-title white-text medium-font heavy-font">
                    A Friend For... Marlow
                </span>
        </div>
        <div>
            <div class="dog-dropdown position-relative">
                <img class="dog-dropdown-menu-icon active icon margin-10-left" src="{{ asset('icons/paw.png') }}" alt="">
                <div class="dog-dropdown-wrapper dark-grey-text pad-20-all-round light-grey-bg panel primary-font heavy-font medium-font">
                    <div class="large-font theme-text pad-10-bottom">Hi, Jessica!</div>
                    <div class="dark-grey-text small-font pad-5-top-n-bottom">Your dogs are below. If you need any help, please click help.</div>
                    <hr/>
                    <ul class="dog-list">
                        <li class="pad-10-top-n-bottom single-dog"><img class="icon margin-10-right" src="{{ asset('icons/single-dog.png') }}" alt=""> Poppy</li>
                        <li class="pad-10-top-n-bottom single-dog active"><img class="icon margin-10-right" src="{{ asset('icons/single-dog.png') }}" alt=""> Marlow</li>
                        <li class="pad-10-top-n-bottom single-dog"><img class="icon margin-10-right" src="{{ asset('icons/single-dog.png') }}" alt=""> Jennie</li>
                    </ul>
                    <hr/>
                    <ul class="options-list">
                        <li class="pad-5-top-n-bottom single-dog">Settings</li>
                        <li class="pad-5-top-n-bottom single-dog">Help</li>
                    </ul>
                    <hr/>
                    <ul class="options-list">
                        <li class="pad-5-top-n-bottom single-dog">Log Out</li>
                    </ul>
                </div>
            </div>

        </div>

    </div>
</div>